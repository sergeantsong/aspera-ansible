#!/bin/bash

yum install git ansible -y
git clone https://bitbucket.org/sergeantsong/aspera-ansible.git
cd aspera-ansible
ansible-playbook -i hosts shares.yml
