#!/usr/bin/python

# -*- coding: utf-8 -*-
#
# Copyright 2016 Aspera - IBM Company
#
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

from ansible.module_utils.basic import *

def set_asconfigurator(module, cfg):
    cmd = ['/opt/aspera/bin/asconfigurator']
    cmd.append('-x')
    cmd.append(cfg)
    is_error, output, errors= module.run_command(cmd)
    if is_error == 0:
        return False, output, errors
    else:
        return True, output, errors


def main():
    module = AnsibleModule(
        argument_spec = dict(
            server_name = dict(required=False, type='str'),
            
            user_name = dict(required=False, type='str'),
            user_docroot = dict(required=False, type='str'),
            token_encryption_key = dict(required=False, type='str'),
            authorization_transfer_in_value = dict(required=False, default='token', type='str'),
            authorization_transfer_out_value = dict(required=False, default='token', type='str'),

            persistent_store = dict(required=False, choices=['disable','enable'], type='str'),

            https_port= dict(required=False, type='str'),
            enable_https= dict(required=False, type='str')
        )
    )
#    asconfigurator_cmd = '/opt/aspera/bin/asconfigurator -x' 
#
#    section = module.params['section']
#    server_option = module.params['server_option']
#    user_option = module.params['user_option']
#    cfg = '\'set_server_data;server_name,\'' + server_name
#    cmd = asconfigurator_cmd + ' ' + cfg
#    output = module.run_command(cmd)

#    choice_map = {
#        'server': set_server_data,
#        'user': set_user_data,
#    }

    #module = AnsibleModule(argument_spec=fields)
    #is_error, has_changed, result = choice_map.get(
    #    module.params['section'])(module.params, module)  
    required_together=[
      [ 'user_docroot', 'user_name' ]
      ]

   # server_name = module.params['server_name'] if module.params['server_name']
   # user_name = module.params['user_name'] if module.params['user_name']
   # user_docroot = module.params['user_docroot'] if module.params['user_docroot']
   # enable_https = module.params['enable_https'] if module.params['enable_https']
   # https_port = module.parms['https_port'] if module.parms['https_port']
   # persistent_store = module.parms['persistent_store']

    server_name = module.params['server_name']

    user_name = module.params['user_name'] 
    user_docroot = module.params['user_docroot'] 
    token_encryption_key = module.params['token_encryption_key']
    authorization_transfer_in_value = module.params['authorization_transfer_in_value']
    authorization_transfer_out_value = module.params['authorization_transfer_out_value']

    persistent_store = module.params['persistent_store']

    enable_https = module.params['enable_https']
    https_port = module.params['https_port'] 



# Need to implement
#
# set_user_data
# set_group_data
# set_node_data
# set_trunk_data
# set_database_data
# set_server_data
# set_client_data
# set_provider_data
# set_central_server_data
# set_http_server_data


#    user = {}
#    user.update('user_name', user_name)
#    user.update('user_docroot',user_docroot)
#
#    server = {}
#    server.update('server_name', server_name)
#
#    central = {}
#    central.update('persistent_store', persistent_store)
#
#    http = {}
#    http.update('enable_https', enable_https)
#    http.update('https_port', https_port)

    if server_name:
        cfg = "set_server_data;server_name," + server_name
        is_error, output, errors = set_asconfigurator(module, cfg)
        if is_error:
            module.fail_json(msg='Error Setting server_name', result=output)
      
    if user_docroot and user_name:
        cfg = "set_user_data;user_name,"+ user_name + ";absolute," + user_docroot
        if os.path.exists(user_docroot):
            is_error, output, errors = set_asconfigurator(module, cfg)
            if is_error:
                module.fail_json(msg='Error setting user_docroot', result=output)
        else:
            module.fail_json(msg='Docroot doesn\'t exist', result=output)
    if user_name:
        cfg = "set_user_data;user_name," + user_name
        is_error, output, errors = set_asconfigurator(module, cfg)
        if is_error:
            module.fail_json(msg='Error Setting user_name', result=output)
    if persistent_store:
        cfg = 'set_central_server_data;persistent_store,' + persistent_store
        is_error, output, errors = set_asconfigurator(module, cfg)
        if is_error:
            module.fail_json(msg='Error setting persistent_store', result=output)

    if enable_https:
        cfg = 'set_http_server_data;enable_https,' + enable_https
        is_error, output, errors = set_asconfigurator(module, cfg)
        if is_error:
            module.fail_json(msg='Error setting enable_https', result=output)


    module.exit_json(changed=True)


if __name__ == '__main__':
    main()
